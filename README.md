# 分布式序列号生成组件

#### 项目介绍
微服务时代，我们需要生产一个连续的序列号，变得比较麻烦。
这里使用了Mysql实现了一个简单的分布式序列号生成组件。后续还可以支持redis等其他高效中间件。
当然还有一种叫雪花算法的序列号生成器，这种算法有个缺点就是长度太长，不是真正意义的从1开始累加序列。
当然秉着包容万象的心态，后续可以实现进去，让用户自己根据特定场景选择算法。

#### 软件架构
DB模块实现原理很简单，在mysql数据专门创建一张表，例如：
<pre><code>
CREATE TABLE IF NOT EXISTS `sequence`(
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `value` bigint(20) NOT NULL COMMENT 'sequence当前值',
    `name` varchar(32) NOT NULL COMMENT 'sequence对应的名称，通常是表名',
    `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
    `gmt_modified` DATETIME NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`ID`) ,UNIQUE uk_name (`name`)
);
</pre></code>

每钟业务类型创建一条记录，value表示当时取的区间值，例如当前value值是100。
当我们设置step长为100时，来取一次，就会update到value。然后程序节点就可以把这个100分配出去。

#### 代码结构

1. xsequence-core：核心代码
2. xsequence-test：测试代码

#### Maven支持
<pre><code>
&lt;dependency>
    &lt;groupId>com.xuanner&lt;/groupId>
    &lt;artifactId>xsequence-core&lt;/artifactId>
    &lt;version>1.1&lt;/version>
&lt;/dependency>
</pre></code>

#### 历史版本说明
v1.0   <br/>
新加特性：支持DB方式生成序列号<br/>
使用文档：https://my.oschina.net/u/1271235/blog/1808103<br/>
更新时间：2018/05/07
===================================================================================<br/>
v1.1   <br/>
新加特性：支持Redis方式生成序列号<br/>
使用文档：https://my.oschina.net/u/1271235/blog/1809437<br/>
更新时间：2018/05/09
===================================================================================<br/>

#### 联系方式
1. 姓名：徐安
2. 邮箱:javaandswing@163.com
3. QQ：349309307
4. 个人博客：xuanner.com