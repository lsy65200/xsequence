package com.xuanner.seq.sequence.impl;

import com.xuanner.seq.exception.SeqException;
import com.xuanner.seq.range.SeqRange;
import com.xuanner.seq.range.SeqRangeMgr;
import com.xuanner.seq.sequence.RangeSequence;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 获取序列号通用接口默认实现
 * Created by xuan on 2018/1/10.
 */
public class DefaultRangeSequence implements RangeSequence {

    private final Lock lock = new ReentrantLock();

    private SeqRangeMgr seqRangeMgr;

    private volatile SeqRange currentRange;

    private String name;

    @Override
    public long nextValue() throws SeqException {
        if (null == currentRange) {
            lock.lock();
            try {
                if (null == currentRange) {
                    currentRange = seqRangeMgr.nextRange(name);
                }
            } finally {
                lock.unlock();
            }
        }

        long value = currentRange.getAndIncrement();
        if (value == -1) {
            lock.lock();
            try {
                for (; ; ) {
                    if (currentRange.isOver()) {
                        currentRange = seqRangeMgr.nextRange(name);
                    }

                    value = currentRange.getAndIncrement();
                    if (value == -1) {
                        continue;
                    }

                    break;
                }
            } finally {
                lock.unlock();
            }
        }

        if (value < 0) {
            throw new SeqException("Sequence value overflow, value = " + value);
        }

        return value;
    }

    @Override
    public void setSeqRangeMgr(SeqRangeMgr seqRangeMgr) {
        this.seqRangeMgr = seqRangeMgr;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

}
