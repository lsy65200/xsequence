package com.xuanner.seq.sequence;

import com.xuanner.seq.exception.SeqException;

/**
 * 获取序列号通用接口
 * Created by xuan on 2018/1/10.
 */
public interface Sequence {

    /**
     * 取下一个值
     *
     * @return 序列值
     * @throws SeqException 异常
     */
    long nextValue() throws SeqException;
}
