package com.xuanner.seq.range.impl.redis;

import com.xuanner.seq.exception.SeqException;
import com.xuanner.seq.range.SeqRange;
import com.xuanner.seq.range.SeqRangeMgr;
import redis.clients.jedis.Jedis;

/**
 * Redis区间管理器
 * Created by xuan on 2018/5/8.
 */
public class RedisSeqRangeMgr implements SeqRangeMgr {

    /**
     * 前缀防止key重复
     */
    private final static String KEY_PREFIX = "x_sequence_";

    private Jedis jedis;

    /**
     * IP
     */
    private String  ip;
    /**
     * PORT
     */
    private Integer port;

    /**
     * 验证权限
     */
    private String auth;

    /**
     * 区间步长
     */
    private int step = 1000;

    @Override
    public SeqRange nextRange(String name) throws SeqException {
        Long max = jedis.incrBy(getRealKey(name), step);
        Long min = max - step + 1;
        return new SeqRange(min, max);
    }

    @Override
    public void init() {
        checkParam();
        jedis = new Jedis(ip, port);
        if (null != auth) {
            jedis.auth(auth);
        }
    }

    private void checkParam() {
        if (isEmpty(ip)) {
            throw new SecurityException("[RedisSeqRangeMgr-checkParam] ip is empty.");
        }
        if (null == port) {
            throw new SecurityException("[RedisSeqRangeMgr-checkParam] port is null.");
        }
    }

    private String getRealKey(String name) {
        return KEY_PREFIX + name;
    }

    private boolean isEmpty(String str) {
        return null == str || str.trim().length() == 0;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
